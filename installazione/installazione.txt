Eseguire Maven Package. Questo creerà un file amp nella directory target.
Per procedere all'installazione è necessario copiare i file generati nella directory
target/amp/lib in $SHARE_WEBAPP_ROOT/WEB-INF/lib. I file da copiare sono:

jackson-annotations-2.9.6.jar
jackson-core-2.9.6.jar
jackson-databind-2.9.6.jar
scribejava-core-5.5.0.jar
share-wordpress-oauth2-1.0-SNAPSHOT.jar

In $SHARE_WEBAPP_ROOT/WEB-INF/web.xml aggiungere il seguente filtri:

   <filter>
        <description>Wordpress Oauth Authentication Filter</description>
        <filter-name>WordpressSsoFilter</filter-name>
        <filter-class>com.alkemy.alfresco.sso.WordpressSsoFilter</filter-class>
   </filter>

   <filter-mapping>
       <filter-name>WordpressSsoFilter</filter-name>
       <url-pattern>/page/*</url-pattern>
   </filter-mapping>
   <filter-mapping>
       <filter-name>WordpressSsoFilter</filter-name>
       <url-pattern>/p/*</url-pattern>
   </filter-mapping>

In $ALFRESCO_ROOT/tomcat/shared/classes/alfresco/web-extension modificare il file
share-config-custom.xml ed aggiungere le seguenti linee, opportunamente modificate,
all'interno di <alfresco-config>:

        <config evaluator="string-compare" condition="WordpressSsoFilter"> <!-- the condition must always be OAuthFilter -->
            <repository>
                <!-- The host of the Alfresco repository webapp -->
                <host>localhost</host>
                <!-- The port of the Alfresco repository webapp. Put 80 for standard HTTP-->
                <port>9090</port>
                <!-- The protocol to access the Alfresco repository -->
                <protocol>http</protocol>
                  <!-- The API access URI. If you use standard Alfresco, this should not change -->
                <api>/alfresco/service/api</api>
                   <!-- The admin user who is able to create new users -->
                <admin>admin</admin>
                <!-- The password of the admin user -->
                <password>admin</password>
                <!-- The unique password for all users authenticated with OAuth. Choose one very complicated :) -->
                <user-password>78637yd7HSxaS_as7gadG7s</user-password>
            </repository>
            <wordpressSSo>
			     <uri>http://206.189.61.119/</uri>
                 <clientId>K3twsAsKciPrK2xzBNYkQxGNOLOe8RYSVson3R7f</clientId>
                 <secret>9XHUZftB2sVTZCGNnv2vm1ohtMFwENMNKuIKJblM</secret>
				 <callback>http://localhost:9090/share/page</callback>
            </wordpressSSo>
        </config>

L'utente admin presente in alfresco NON può effettuare in login attraverso SSO OAuth2, per una
questione di security, per tanto, per eleggere un utente SSO come admin èè necessario accedere ad
alfresco con le credenziali di admin bypassando oauth2, ed aggiungere l'utente designato al gruppo
ALFRESCO_ADMINISTRATORS.

Per fare questo è necessario collegarsi all'url http://URL_ALFRESCO_SHARE/share/page?bypassOAuth=true

Il funzionamento dell'SSO è questo:

    1) Il filtro verifica che l'utente è autenticato, se lo è continua come sempre
    2) Il filtro verifica che è presente il parametro di bypass ouath, se c'è visualizza la schermata
       di login di alfresco share di default
    3) Il filtro verifica che ci sia il parametro del token, se non c'è effettua il redirect
       verso la pagina di login Wordpress
    4) Il filtro prende i parametri di login ed estrae l'username. Se non è presente nel database
       utente, l'aggiunge. Se è presente conferma che l'utente autenticato è la username ricevuta.
       Procede quindi alla visualizzazione classica di share.

Limitazione: Alfresco Richiede che siano presenti obbligatoriamente i parametri firstname e lastname,
mentre il plugin OAuth2 restituisce il cognome e il nome come campo unico, per tanto in uno dei due
campi è stato aggiunta la scritta "Utente".

