package com.alkemy.alfresco.sso;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.extensions.config.Config;
import org.springframework.extensions.config.ConfigElement;
import org.springframework.extensions.config.ConfigService;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;


public class OAuthConfigurator {

    private static final String REPOSITORY_PROTOCOL = "repository.protocol";
    private static final String REPOSITORY_HOST = "repository.host";
    private static final String REPOSITORY_PORT = "repository.port";
    private static final String REPOSITORY_API = "repository.api";
    private static final String REPOSITORY_ADMIN_USER = "repository.admin";
    private static final String REPOSITORY_ADMIN_PASSWORD = "repository.password";
    private static final String USER_PASSWORD = "repository.user-password";


    private static final String API_CLIENTID = "wordpressSSo.clientId";
    private static final String API_URI = "wordpressSSo.uri";
    private static final String API_SECRET = "wordpressSSo.secret";
    private static final String API_CALLBACK = "wordpressSSo.callback";

    public static final String REPOSITORY_API_PEOPLE = "people";
    public static final String REPOSITORY_API_LOGIN = "login";
    public static final String DEFAULT_TICKET_NAME = "alf_ticket";


    private String wordpressUrl;
    private String clientId;
    private String clientSecret;
    private String callbackUrl;
    private String userPassword;
    private String apiUri;
    private String adminUser;
    private String adminPassword;

    private ApplicationContext applicationContext;

    private static Log logger = LogFactory.getLog(OAuthConfigurator.class);

    private static OAuthConfigurator _instance;

    public static OAuthConfigurator getInstance(ApplicationContext applicationContext) {
        if (_instance == null) {
            _instance = new OAuthConfigurator(applicationContext);
        }
        return _instance;
    }

    private OAuthConfigurator(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        getConfig();
    }

    private void getConfig() {
        wordpressUrl = getConfigValue(API_URI);
        clientId = getConfigValue(API_CLIENTID);
        clientSecret = getConfigValue(API_SECRET);
        callbackUrl = getConfigValue(API_CALLBACK);
        userPassword = getConfigValue(USER_PASSWORD);
        apiUri = getConfigValue(REPOSITORY_PROTOCOL) +
                "://" +
                getConfigValue(REPOSITORY_HOST) +
                ":" +
                getConfigValue(REPOSITORY_PORT) +
                getConfigValue(REPOSITORY_API) +
                "/";

        adminPassword = getConfigValue(REPOSITORY_ADMIN_PASSWORD);
        adminUser = getConfigValue(REPOSITORY_ADMIN_USER);

        logger.info("Wordpress SSO URL:" + wordpressUrl);
        logger.info("Wordpress SSO clientId:" + clientId);
        logger.info("Wordpress SSO clientSecret:" + clientSecret);
        logger.info("Wordpress SSO CallbackURL:" + callbackUrl);
        logger.info("Wordpress SSO ApiUri:" + apiUri);

    }

    private String getConfigValue(String name) {
        ConfigService configService = (ConfigService) applicationContext.getBean("web.config");
        Config oauthConfig = configService.getConfig("WordpressSsoFilter");
        if (oauthConfig == null) {
            throw new IllegalStateException("OAuth Filter has no configuration");
        }

        String[] parts = StringUtils.split(name, '.');

        ConfigElement mainConfig = oauthConfig.getConfigElement(parts[0]);
        if (mainConfig == null) {
            return null;
        }

        ConfigElement subConfig = mainConfig.getChild(parts[1]);
        if (subConfig == null) {
            return null;
        }

        return subConfig.getValue();
    }

    public String getWordpressUrl() {
        return wordpressUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public String getApiUri(String service) {
        return apiUri + service;
    }

    public String getAdminUser() {
        return adminUser;
    }

    public String getAdminPassword() {
        return adminPassword;
    }
}
