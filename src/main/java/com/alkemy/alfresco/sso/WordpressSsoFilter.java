package com.alkemy.alfresco.sso;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethodBase;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.extensions.surf.UserFactory;
import org.springframework.extensions.surf.site.AuthenticationUtil;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Random;

public class WordpressSsoFilter implements Filter {

    private ServletContext servletContext;
    private OAuthConfigurator oAuthConfigurator;
    private ObjectMapper objectMapper;
    private static Log logger = LogFactory.getLog(WordpressSsoFilter.class);


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.info("Inizializzazione WordpresSSo Filter");
        this.servletContext = filterConfig.getServletContext();
        this.oAuthConfigurator=OAuthConfigurator.getInstance(getApplicationContext());
        this.objectMapper = new ObjectMapper();
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {

         HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getParameter("bypassOAuth") != null || request.getSession().getAttribute("share.bypassOAuth") != null) {
            request.getSession().setAttribute("share.bypassOAuth", true);
            chain.doFilter(servletRequest, servletResponse);
            return;
        }


        if (AuthenticationUtil.isAuthenticated(request)) {
                chain.doFilter(request, response);
            return;
        }

        String username = this.doOAuthAuthentication(request, response);
        if (username != null) {
            UserFactory userFactory = (UserFactory) getApplicationContext().getBean("user.factory");
            boolean authenticated = userFactory.authenticate(request, username, oAuthConfigurator.getUserPassword());
            if (authenticated) {
                AuthenticationUtil.login(request, response, username);
            }
        }
        chain.doFilter(servletRequest, servletResponse);
    }


    protected String doOAuthAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String code = request.getParameter("code");
        String state = request.getParameter("state");

        logger.debug("Code: "+code);
        logger.debug("State:"+state);


        if (code==null || code.isEmpty()) {
            String secretState = "secret" + new Random().nextInt(999_999);

            WpOAuthApi.instance().setUrl(oAuthConfigurator.getWordpressUrl());

            OAuth20Service service = new ServiceBuilder(oAuthConfigurator.getClientId())
                    .apiSecret(oAuthConfigurator.getClientSecret())
                    .state(secretState)
                    .callback(oAuthConfigurator.getCallbackUrl())
                    .build(WpOAuthApi.instance());

            response.sendRedirect(service.getAuthorizationUrl());
        } else {
            WordpressUser returner = null;
            try {

                WpOAuthApi.instance().setUrl(oAuthConfigurator.getWordpressUrl());

                OAuth20Service service = new ServiceBuilder(oAuthConfigurator.getClientId())
                        .apiSecret(oAuthConfigurator.getClientSecret())
                        .state(state)
                        .callback(oAuthConfigurator.getCallbackUrl())
                        .build(WpOAuthApi.instance());

                OAuth2AccessToken accessToken = service.getAccessToken(code);

                OAuthRequest requestOauth = new OAuthRequest(Verb.GET, oAuthConfigurator.getWordpressUrl() + "/oauth/me/");
                service.signRequest(accessToken, requestOauth);
                Response responseOauth = service.execute(requestOauth);

                if (responseOauth.getCode() == 200) {
                    returner = objectMapper.readValue(responseOauth.getBody(), WordpressUser.class);
                }

                if (returner!=null) {
                    String username = returner.getUserLogin();

                    String adminTicket = this.getAdminAlfrescoTicket();
                    boolean newUser = !userExists(username, adminTicket);
                    return this.saveUser(username, returner, adminTicket, newUser);

                }

            } catch (Exception ex) {
                logger.error(ex.toString());
            }

        }

        return null;
    }

    protected void addTicketParameter(HttpMethodBase method, String ticket) {
        method.setPath(method.getPath() + "?" + OAuthConfigurator.DEFAULT_TICKET_NAME + "=" + ticket);
    }

    protected String saveUser(String username, WordpressUser userInfo, String adminTicket, boolean newUser) throws IOException {

        EntityEnclosingMethod saveUserMethod;
        if (newUser) {
            saveUserMethod = new PostMethod(oAuthConfigurator.getApiUri(OAuthConfigurator.REPOSITORY_API_PEOPLE));
        } else {
            saveUserMethod = new PutMethod(oAuthConfigurator.getApiUri(OAuthConfigurator.REPOSITORY_API_PEOPLE) + "/" + username);
        }

        this.addTicketParameter(saveUserMethod, adminTicket);

        String input = "{ " +
                (newUser ? "\"userName\" : \"" + username + "\", " : "") +
                "\"firstName\" : \"" + userInfo.getDisplayName() + "\", " +
                "\"lastName\" : \"Utente\", " +
                "\"email\" : \"" + userInfo.getEmail() + "\", " +
                (newUser ? "\"password\" : \"" + oAuthConfigurator.getUserPassword() + "\"" : "") +
                " }";

        saveUserMethod.setRequestEntity(new StringRequestEntity(input, "application/json", "utf-8"));
        HttpClient client = new HttpClient();

        return client.executeMethod(saveUserMethod) == HttpStatus.SC_OK ? username : null;
    }

    protected boolean userExists(String username, String adminTicket) throws IOException {
        GetMethod get = new GetMethod((oAuthConfigurator.getApiUri(OAuthConfigurator.REPOSITORY_API_PEOPLE) + "/" + username));

        this.addTicketParameter(get, adminTicket);
        HttpClient client = new HttpClient();
        boolean exists = client.executeMethod(get) == HttpStatus.SC_OK;
        String userInfo = get.getResponseBodyAsString();
        return (exists);
    }

    protected String getAdminAlfrescoTicket() throws IOException {
        HttpClient client = new HttpClient();
        PostMethod method = new PostMethod(oAuthConfigurator.getApiUri(OAuthConfigurator.REPOSITORY_API_LOGIN));

        String input = "{ " +
                "\"username\" : \"" + oAuthConfigurator.getAdminUser() + "\", " +
                "\"password\" : \"" + oAuthConfigurator.getAdminPassword() + "\" " +
                "}";
        method.setRequestEntity(new StringRequestEntity(input, "application/json", "utf-8"));
        int statusCode = client.executeMethod(method);

        if (statusCode != HttpStatus.SC_OK) {
            return null;
        }

        TicketInfo ticket = objectMapper.readValue(method.getResponseBodyAsString(), TicketInfo.class);
        return ticket.data.ticket;
    }

    @Override
    public void destroy() {

    }


    private ApplicationContext getApplicationContext() {
        return WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }
}
